# nba-docs


## Material for MkDocs

Material for MkDocs: [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)


## macros plugin

[macros plugin](https://github.com/fralau/mkdocs_macros_plugin)

Standard installation:

```bash
pip install mkdocs-macros-plugin
```

Declare the plugin in the the file `mkdocs.yml`:

```yaml
plugins:
    - search
    - macros
```

NOTE: If you have no plugins entry in your config file, you should also add the search plugin. If no plugins entry is set, MkDocs enables search (which is a built-in plugin) by default. However, when you want to use it, you have to declare it explicitly.

## Environment Variables

Required environment variables are:
- SCRATCHPAD_URL
- SWAGGER_UI_URL
- NBA_BASE_URL


## Usage

Build the container:

```bash
docker build . -t nba-docs
```

Writing / testing:
```bash
docker run --name nba-docs \
		-e "NBA_BASE_URL=https://api.biodiversitydata.nl" \
		-e "SCRATCHPAD_URL=https://api.biodiversitydata.nl/scratchpad/" \
		-e "SWAGGER_UI_URL=https://docs.biodiversitydata.nl/endpoints-reference/" \
		--rm -it \-p 8000:8000 \
		-v ${PWD}:/docs nba-docs serve \
		--dev-addr=0.0.0.0:8000
```

Build / publish:
```bash
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs nba-docs build
```
The static html website will be published in the `/site` folder.


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`
