---
title: Known Issues
---

* 27-10-2020: When using a `nameResolutionRequest` clause with the `queryWithNameResolution` endpoint, the query generates an error when `useCoL` is set to `true` and `nameTypes` contains the value `VERNACULAR_NAME`. You can still look up vernacular names using the internal resolver, by setting `useCoL` to `false`.

* 11-10-2017: If the field `gatheringEvent.siteCoordinates.geoShape` is used as a filter, the field is not in the output. See for example [this query](http://api.biodiversitydata.nl/v2/specimen/query/?unitID=L.3119906&_fields=gatheringEvent.siteCoordinates.geoShape).

* 26-10-2017: Live examples in the {{ swagger_ui_link("NBA API endpoint reference") }} for POST methods have empty values as default. The user must provide his own QuerySpec JSON to make the POST examples work.

