---
title: Geographical Data Services
---

Geographical mapping of species occurrences is essential in biogeographical analyses. More than 1.2 Million of our specimen records are geo-referenced, i.e. they have latitude and longitude coordinates reported for the sita at which they were collected. The data type geo stores geographical areas to which the geo-referenced species can be mapped. This enables querying for specimens found in a specific region, to e.g. retrieve all the primate species collected on Madagascar. The areas are represented as polygons of longitude/latitude pairs. All polygons are coded in the GeoJSON format (see below).


## Base URL

The base URL for geo-area specific services is {{ nba_link_text(geo) }}


## Data Source Systems

The data is comprised of:

* a list of countries, source: [https://gadm.org](https://gadm.org/)
* detailed geographical data of the Netherlands, including provinces and municipalities, source: [https://www.cbs.nl](https://www.cbs.nl/)
* a comprehensive list of nature reserves: [https://www.natura2000.nl](https://www.natura2000.nl/), [https://www.staatsbosbeheer.nl](https://www.staatsbosbeheer.nl/), [https://www.natuurmonumenten.nl](https://www.natuurmonumenten.nl/), [https://www.pwn.nl](https://www.pwn.nl/), [https://www.landschapnoordholland.nl](https://www.landschapnoordholland.nl/).


## Available Services

### Query

Querying for geographical data can be done using the {{ swagger_ui_path_link("/geo/query/ endpoint","#/geo/queryHttpGet") }},
which accepts human-readable query strings and JSON encoded [QuerySpec](/advanced-queries/#complex-queries) parameters.

### Data access
Several access methods offer the convenient retrieval of geo areas matching a certain identifier. The services {{ swagger_ui_path_link("/geo/find/","#/geo/find_1") }} and {{ swagger_ui_path_link("/find/findByIds/","#/geo/findByIds_1") }} retrieve records according to their `id` fields (see [here](#identifiers)).


### Metadata

Metadata services provide miscellaneous information about geo area records. This includes detailed information about all {{ swagger_ui_path_link("fields","#/geo/getFieldInfo") }} and {{ swagger_ui_path_link("paths","#/geo/getPaths") }}. A description of all geo area metadata services can be found {{ swagger_ui_path_link("here","#/geo") }}.


## Identifiers

The field `sourceSystemId` of a geo area is the identifier as it is in the source database. A unique identifier consisting of `{sourceSystemId}@{sourceSystem.code}` is stored in the field `id`, e.g. `1003896@GEO`.


## GeoJSON

Polygons of geographic regions as well as point occurrences of specimen are represented in the GeoJSON format ([https://en.wikipedia.org/wiki/GeoJSON](https://en.wikipedia.org/wiki/GeoJSON)), a geospatial data-exchange standard. GeoJSON allows for encoding various types of shapes. Regions and point occurrences are represented by the GeoJSON types MultiPolygon and Point, respectively. In geo areas, the information is stored in the field `shape`, for instance:

{{ nba_link("geo/query/?\_size=1&\_fields=shape") }}

If a specimen is geo-referenced, the GeoJSON can be found in the field `gatheringEvent.siteCoordinates.geoShape`, for example 

{{ nba_link("specimen/query/?unitID=L.1213823&\_fields=gatheringEvent.siteCoordinates") }}

Note that GeoJSON encodes a coordinate as a `[longitude, latitude]` array and that all GeoJSON coordinates in the NBA represent decimal degrees. 


### Query for area type and Locality

The NBA offers polygon data for the following areas (field areaType). Countries (world), and Nature, Municipality and Province (NL). Entries with the `areaType` Nature are nature reserves in the Netherlands, such as the Veluwe, a push moraine in Gelderland. The field locality gives the name of the locality. For example. To retrieve the polygon outlining the country Bermuda, we query for locality: 

{{ nba_link("geo/query/?locality=Bermuda") }}


### Searching in geo shapes

Shape fields in geo areas can be compared using the operators `IN` and `NOT_IN`, telling us if one point or polygon is fully contained in another, or not. For this, it is not necessary to use a GEOJson representation in the query; this can be done with either the id of a geo area record or its locality field.

Example: Retrieve all Nature reserves located in the Dutch province “Noord-Holland”:

```JSON
{
  "conditions": [
    {
      "field": "areaType",
      "operator": "EQUALS",
      "value": "Nature"
    },
    {
      "field": "shape",
      "operator": "IN",
      "value": "Noord-Holland"
    }
  ]
}
```

We would get the same results when querying with the id of the area Noord-Holland (`"value": "1003483@GEO"`). 

Likewise, we can look for specimens that have coordinates in a certain area. Suppose that we want to get all species collected in the country Bermuda. This time, we search for specimens, using either the search term “Bermuda” or the id for Bermuda (`1003922@GEO`):

```JSON
{
  "conditions": [
    {
      "field": "gatheringEvent.siteCoordinates.geoShape",
      "operator": "IN",
      "value": "Bermuda"
    }
  ]
}
```

### Searching in custom GeoJSON Areas

It is also possible to enter a GeoJSON directly into a query term. Suppose that we want to query for all specimens that were collected in the *Bermuda Triangle*. However, the Bermuda Triangle is not available as a geo shape in the NBA, so we cannot search using locality or id but must specify a custom location. The GeoJSON describing the Bermuda triangle is as follows:

```JSON
{
  "type": "MultiPolygon",
  "coordinates": [
    [
      [
        [
          -64.73,
          32.31
        ],
        [
          -80.19,
          25.76
        ],
        [
          -66.09,
          18.43
        ],
        [
          -64.73,
          32.31
        ]
      ]
    ]
  ]
}
```

We can then query for specimens using the JSON string:

```JSON
{
  "conditions": [
    {
      "field": "gatheringEvent.siteCoordinates.geoShape",
      "operator": "IN",
      "value": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[-64.73, 32.31],[-80.19, 25.76],[-66.09, 18.43],[-64.73, 32.31]]]]}"
    }
  ]
}
```

Note that the quotes in the GeoJSON string have to be escaped and that linebreaks are removed. This query will yield specimens that have been collected within the boundaries of the Bermuda Triangle. 
