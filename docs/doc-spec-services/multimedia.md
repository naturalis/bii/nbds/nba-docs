---
title: Multimedia Data Services
---


Naturalis stores a vast amount of multimedia in its internal document store.
This can be multimedia captured from physical specimens 
(e.g. photos of a specimen),
but also from human observations 
(e.g. audio recordings of wildlife sounds).
Digitisation of Naturalis museum objects include a detailed photographic documentation. 
So far, there are more than 13 million photos 
and several hundreds of thousands of wildlife sounds.


## Base URL

The base URL for multimedia-specific services is {{ nba_link_text("multimedia") }}


## Data Source Systems

Multimedia data are retrieved from our in-house registration systems for botanical (BRAHMS) or zoological/geological (CRS) specimens, and from our external partner Observation International. In 2018, we added the records from [Xeno-canto](https://www.xeno-canto.org/), the most comprehensive database of wildlife sounds to date. A small fraction of items is retrieved from both the [Dutch Species Register](http://www.nederlandsesoorten.nl/), and the [Dutch Caribbean Species Register](https://www.dutchcaribbeanspecies.org/).


## Available Services

### Query

Querying for multimedia data can be done using the {{ swagger_ui_path_link("/multimedia/query/ endpoint","#/multimedia/queryHttpGet_1") }}, which accepts human-readable query strings and JSON encoded [QuerySpec](/advanced-queries/#complex-queries) parameters.


### Data Access

Several access methods offer the convenient retrieval of multimedia records matching a certain identifier. The services {{ swagger_ui_path_link("/multimedia/find/","#/multimedia/find_2") }} and {{ swagger_ui_path_link("/multimedia/findByIds/","#/multimedia/findByIds_2") }} retrieve taxa according to their `id` fields (see [here](#multids)).


### Metadata

Metadata services provide miscellaneous information about multimedia records. This includes detailed information about all {{ swagger_ui_path_link("fields","#/multimedia/getFieldInfo_1") }} and {{ swagger_ui_path_link("paths","#/multimedia/getPaths_1") }}. A description of all multimedia metadata services can be found {{ swagger_ui_path_link("here","#/multimedia") }}.


## Identifiers

The field `sourceSystemId` of a geo area is the identifier as it is in the source database. The `sourceSystemId`, in turn, consists of the specimen `unitID` for which media was recorded, and an internal identifier(for example `RMNH.AVES.140023_2`).  A unique identifier consisting of `{sourceSystemId}@{sourceSystem.code}` is stored in the field `id`, e.g. `RMNH.AVES.140023_2@CRS`.


## Link to Specimen and Taxa

Each multimedia record is linked either to the id of a specimen or a taxon via the fields `associatedSpecimenReference` and `associatedTaxonReference`, respectively. In order to provide a faster search for specific specimen/taxon attributes within multimedia content, their identifications block is included in a multimedia record. For example, one can search for the pygmy cormorant, given its species name *Phalacrocorax pygmeus*:

{{ nba_link("multimedia/query/?identifications.defaultClassification.genus=Phalacrocorax&identifications.defaultClassification.specificEpithet=pygmeus") }}


## Downloading Multimedia Content

Each multimedia record has one or more download URLs retrievable via the field(s) `serviceAccessPoints.accessURI`. Below we query for the image location of the pygmy cormorant:

{{ nba_link("multimedia/query/?identifications.defaultClassification.genus=Phalacrocorax&identifications.defaultClassification.specificEpithet=pygmeus&\_fields=serviceAccessPoints.accessUri") }}

<figure>
<div style="text-align: center;">
	<p><img src="http://medialib.naturalis.nl/file/id/ZMA.AVES.38187/format/large" align="center"
		alt="pigmy-cormorant" width=300>
		<figcaption>Museum specimen of the Pygmy cormorant (Phalacrocorax pygmeus)</figcaption>
	</div>
</figure>


## Artistic Images

Next to photos and videos of specimens, the NBA also gives access to about 2500 artistic scientific drawings and lithographs. These documents are categorised in the collection Arts and can be retrieved via the field collectionType. To obtain all drawings picturing species of genus Phalacrocorax, one can query within the Arts collection:

{{ nba_link("multimedia/query/?identifications.defaultClassification.genus=Phalacrocorax&collectionType=Arts") }}

Among the results we find for instance this nice drawing of *Phalacrocorax aristotoles*:

<figure>
<div style="text-align: center;">
	<p><img src="http://medialib.naturalis.nl/file/id/image-134788/format/large" align="center" width=500
    alt="shag-drawing">
  <figcaption>Drawing of the European shag (Phalacrocorax aristotoles)</figcaption>
</div>
</figure>


## Licensing

License details for each multimedia item can be accessed by the fields licenseType and license. Most of them are published under one of the [Creative Commons licenses](https://creativecommons.org/).
